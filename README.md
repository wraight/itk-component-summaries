# itk-component-summaries



## Plan

Repo with notebooks (then scripts):

- Collect component IDs

    - Mirror existing definition
    - Output csv
- Get test history and build summary
    - ComponentType structure
    - Output csv
        - Could be uploaded as test
    - initially pass/fail
- Plot summary data
    - Parallel lines
    - Simple report
- Containerise scripts
    - docker
- Run as cronjob
    - OpenShift
    